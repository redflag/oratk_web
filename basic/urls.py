from django.conf.urls import url, include
from django.contrib import admin
from oratk import views as oratk
from basic import views as basic

urlpatterns = [
    url(r'^$', basic.login),
    url(r'^login', basic.login),
    url(r'^logout', basic.logout),

    url(r'^dashboard$', basic.dashboard),
    url(r'^user$', basic.user),
    url(r'^user_add_edit$', basic.user_add_edit),
    # url(r'^user_search$', basic.user_search),
    # url(r'^user_edit', basic.user_edit),
    url(r'^user_delete', basic.user_delete),
    url(r'^user_lock', basic.user_lock),
    url(r'^user_more', basic.user_more),

    url(r'^instance$', basic.instance),
    url(r'^instance_add_edit$', basic.instance_add_edit),
    # url(r'^instance_search$', basic.instance_search),
    # url(r'^instance_edit', basic.instance_edit),
    url(r'^instance_delete', basic.instance_delete),
    url(r'^instance_lock', basic.instance_lock),
    url(r'^instance_more', basic.instance_more),
    url(r'^instance_ins_check', basic.instance_ins_check),

    url(r'^host$', basic.host),
    url(r'^host_add_edit$', basic.host_add_edit),
    # url(r'^host_search$', basic.host_search),
    # url(r'^host_edit', basic.host_edit),
    url(r'^host_delete', basic.host_delete),
    url(r'^host_lock', basic.host_lock),
    url(r'^host_more', basic.host_more),

    url(r'^app$', basic.app),

]
