# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django_celery_beat import models as beat_models


###后台用户信息表
class user_info(models.Model):
    username = models.CharField(max_length=32,unique=True)
    password = models.CharField(max_length=64)
    status = models.CharField(max_length=10)
    email = models.EmailField()
    crtime = models.DateField(auto_now_add=True)
    chtime = models.DateField(auto_now=True)
    iphone = models.CharField(max_length=11)
    dept = models.CharField(max_length=64)


###数据库实例列表
class instance_info(models.Model):
    dbname = models.CharField(max_length=32)
    ip = models.CharField(max_length=64)
    port = models.CharField(max_length=10)
    conn_string = models.CharField(max_length=64)
    sid = models.CharField(max_length=32)
    service_name = models.CharField(max_length=320)
    status = models.CharField(max_length=10)
    ins_active = models.CharField(max_length=10)
    ins_active_time = models.CharField(max_length=64)
    ins_active_msg = models.CharField(max_length=320)
    type = models.CharField(max_length=10)
    crtime = models.DateField(auto_now_add=True)
    chtime = models.DateField(auto_now=True)
    remark = models.CharField(max_length=64)

###app列表
class app_info(models.Model):
    app_name = models.CharField(max_length=32)
    img = models.CharField(max_length=32)
    content= models.CharField(max_length=64)
    address = models.CharField(max_length=64)
    crtime = models.DateField(auto_now_add=True)
    chtime = models.DateField(auto_now=True)
    remake = models.CharField(max_length=64)

###功能点信息
class oratk_tool_info(models.Model):
    name = models.CharField(max_length=32)
    sqltext= models.CharField(max_length=3200)
    status= models.CharField(max_length=8)
    remake = models.CharField(max_length=64)
    crtime = models.DateField(auto_now_add=True)
    chtime = models.DateField(auto_now=True)





class host_info(models.Model):
    name = models.CharField(max_length=32)
    hostname = models.CharField(max_length=32)
    desc = models.CharField(max_length=32)
    ip = models.CharField(max_length=64)
    port = models.CharField(max_length=64,null=True)
    gm_ip = models.CharField(max_length=64)
    other_ip = models.CharField(max_length=64)
    status = models.CharField(max_length=10)
    type = models.CharField(max_length=10)
    osversion = models.CharField(max_length=32)
    crtime = models.DateField(auto_now_add=True)
    chtime = models.DateField(auto_now=True)
    place = models.CharField(max_length=32,null=True)
    remark = models.CharField(max_length=64,null=True)