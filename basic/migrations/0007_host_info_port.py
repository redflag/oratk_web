# Generated by Django 3.1.3 on 2020-12-14 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('basic', '0006_host_info_place'),
    ]

    operations = [
        migrations.AddField(
            model_name='host_info',
            name='port',
            field=models.CharField(max_length=64, null=True),
        ),
    ]
