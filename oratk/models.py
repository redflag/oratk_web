from django.db import models

# Create your models here.

# monitor 巡检报告（report)
class oratk_most_logmnr_record(models.Model):
    rec_date = models.CharField(max_length=320, null=True) # 执行时间
    db_name = models.CharField(max_length=320, null=True)  # 数据库名字
    db_conn = models.CharField(max_length=320, null=True)  # 数据库连接串
    run_date_range = models.CharField(max_length=320, null=True)  # 挖掘时间段
    run_condition = models.CharField(max_length=320, null=True)   # 实例化条件
    run_table = models.CharField(max_length=320, null=True)  # 实例化表名
    table_count = models.CharField(max_length=320,null=True) # 实例化表中的条目数
    status = models.CharField(max_length=320,null=True)    # 状态
    msg_err = models.CharField(max_length=320, null=True)  # 错误信息


##sqlldr上传文件记录
class oratk_most_sqlldr_uploadfile(models.Model):
    rec_date = models.CharField(max_length=320, null=True) # 执行时间
    file_name = models.CharField(max_length=320, null=True)  # 数据库名字
    file_path = models.CharField(max_length=320, null=True)  # 数据库连接串
    file_size = models.CharField(max_length=320, null=True)  # 挖掘时间段
    status = models.CharField(max_length=32,null=True)    # 状态   1为excel  0为 txt csv
    file_mode = models.CharField(max_length=32,null=True)  #文件格式
    file_sheets_count = models.CharField(max_length=32,null=True)  #excel文件中的sheet个数
    file_make = models.CharField(max_length=32,null=True)  #文件来源   源文件还是
    content_count = models.CharField(max_length=3200,null=True)  #内容行数
    file_sheets = models.CharField(max_length=320, null=True)  # excel文件中的sheet name list
    format_info = models.CharField(max_length=320, null=True)  # 文件format格式

##sqlldr controlfile文件模板
class oratk_most_sqlldr_controlfile(models.Model):
    control_name = models.CharField(max_length=320, null=True)  # 模板名字
    control_content = models.CharField(max_length=3200,null=True)  #模板内容

##sqlldr 导入记录
class oratk_most_sqlldr_record(models.Model):
    ins_info = models.CharField(max_length=320,null=True)  #实例信息
    file_path = models.CharField(max_length=320,null=True)
    control_file_name = models.CharField(max_length=32,null=True)  # 控制文件
    log_file_name = models.CharField(max_length=32,null=True)  # log文件
    bad_file_name = models.CharField(max_length=32,null=True)  # bad文件
    ctime = models.CharField(max_length=32,null=True)  # 执行时间
    etime = models.CharField(max_length=32,null=True)  # 耗时

##sqlldr 控制文件 日志文件记录
class oratk_most_sqlldr_log_file(models.Model):
    file_path = models.CharField(max_length=320,null=True)
    file_name = models.CharField(max_length=320,null=True)  # 文件名字
    file_content = models.TextField(null=True)  # 文件内容
