# -*- coding: utf-8 -*-
"""oratk_web URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from oratk import views as oratk

"""urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/',oratk.login ,name='login'),
]
"""

urlpatterns = [
    # basic_app
    # basic后台url
    #url(r'^$', include("basic.urls")),
    url('', include("basic.urls")),
    url(r'^login/', include("basic.urls")),
    url(r'^basic_app/', include("basic.urls")),
    # url(r'^oratk/', include("oratk.urls") ),
    url(r'^admin/', admin.site.urls),
    # oratk_app
    # oratk url
    url(r'^oratk_app/', include("oratk.urls")),
    url(r'^oratk_cron/', include("cron.urls")),
    url(r'^mytk_app/', include("mytk.urls")),
]