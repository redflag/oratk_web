# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.shortcuts import redirect
from basic import models
from django.db.models import Q,QuerySet
from django.core.paginator import Paginator
from basic.models import instance_info
import ast
from django.http import JsonResponse, HttpResponse
# Create your views here.
from django.forms.models import model_to_dict
import json
import cx_Oracle
import time


###用户登入
def mytk(request):
    error_msg = ""
    if request.method == 'POST':
        user = request.POST.get('user',None)
        pwd = request.POST.get('pwd',None)
        #request.session['username'] = user
        login_obj = models.user_info.objects.filter(username=user,password=pwd).first()
        if login_obj:
            request.session['username'] = user
            request.session['is_login'] = True
            if login_obj.status == 'LOCKED':
                error_msg = "该用户状态为'LOCKED'"
                return render(request, 'basic_app/login.html', {'error_msg': error_msg})
            # return redirect('/basic_app/user')
            return redirect('/oratk_app/oratk_query')
            # return render(request,'/basic_app/user.html',{'userinfo':user})
            #return HttpResponse("登入成功")
        else:
            error_msg = "用户名或密码错误"
            #print request.path
            return render(request, 'basic_app/login.html', {'error_msg': error_msg})
    if request.method == 'GET':
        return render(request, 'basic_app/login.html', {'error_msg':error_msg})