from django.conf.urls import url, include
from django.contrib import admin
from oratk import views as oratk
from mytk import views as mytk

urlpatterns = [
    url(r'^mytk_app$', mytk.mytk),
]
