# oratk_web

#### 介绍
oratk 工具
针对数据库高效运维的实用性工具

#### 软件架构
python django 框架

#### pip包
```shell
amqp==2.6.1
anyjson==0.3.3
asgiref==3.3.1
bcrypt==3.2.0
billiard==3.6.3.0
certifi==2020.12.5
cffi==1.14.4
chardet==3.0.4
click==8.1.3
click-didyoumean==0.3.0
click-plugins==1.1.1
click-repl==0.2.0
cryptography==37.0.2
cx-Oracle==8.1.0
et-xmlfile==1.1.0
idna==2.10
kombu==4.6.11
mysqlclient==2.0.3
numpy==1.22.4
openpyxl==3.0.10
pandas==1.3.5
paramiko==2.7.2
prompt-toolkit==3.0.13
pycparser==2.21
PyMySQL==1.0.2
PyNaCl==1.5.0
python-crontab==2.5.1
python-dateutil==2.8.1
pytz==2020.5
redis==3.5.3
requests==2.25.0
six==1.15.0
sql-metadata==2.6.0
sqlparse==0.4.2
urllib3==1.26.2
vine==1.3.0
wcwidth==0.2.5
Django==3.1.3
celery==4.4.7
django-timezone-field==4.1.1
django-celery-results==1.2.1
django-celery-beat==2.1.0
#django-celery==3.3.1 #celery4不再需要此包
```



#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明
项目主要图例
![Image text](readme_png/overview.jpg)
![Image text](readme_png/user_info.jpg)
![Image text](readme_png/inst_info.jpg)
![Image text](readme_png/host.jpg)
![Image text](readme_png/ora_custom_query.png)
![Image text](readme_png/ora_query1.jpg)
![Image text](readme_png/ora_query2.jpg)
![Image text](readme_png/ora_query3.jpg)
![Image text](readme_png/query_point.jpg)
![Image text](readme_png/switch_redo_point_1.jpg)
![Image text](readme_png/switch_redo_point_2.jpg)
![Image text](readme_png/switch_redo_point_3.jpg)
![Image text](readme_png/oratk_tool_query_1.jpg)
![Image text](readme_png/oratk_tool_query_2.jpg)
![Image text](readme_png/oratk_tool_shell.jpg)
![Image text](readme_png/session_1.jpg)
![Image text](readme_png/session_2.jpg)
![Image text](readme_png/session_3.jpg)
![Image text](readme_png/plan_1.jpg)
![Image text](readme_png/plan_2.jpg)
![Image text](readme_png/logmnr_1.jpg)
![Image text](readme_png/logmnr_2.jpg)
![Image text](readme_png/logmnr_3.jpg)
![Image text](readme_png/awr_ash_11.jpg)
![Image text](readme_png/awr_ash_12.jpg)
![Image text](readme_png/awr_ash_13.jpg)
![Image text](readme_png/archery_1.jpg)
![Image text](readme_png/archery_2.jpg)
![Image text](readme_png/sqlldr.jpg)
![Image text](readme_png/check_1.jpg)
![Image text](readme_png/check_2.jpg)
![Image text](readme_png/cron_1.jpg)
![Image text](readme_png/cron_2.jpg)
![Image text](readme_png/cron_3.jpg)
![Image text](readme_png/cron_4.jpg)
![Image text](readme_png/exec_info.jpg)
![Image text](readme_png/sms_info.jpg)



#### 联系方式
```shell
email: lwk0727@163.com
    v: Liwk09071327
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
