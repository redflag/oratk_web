# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery, platforms
import celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'py_oratk_3.settings')  # 设置django环境

app = Celery('py_oratk_3')
app.config_from_object('django.conf:settings', namespace='CELERY') #  使用CELERY_ 作为前缀，在settings中写配置

# 发现任务文件每个app下的task.py
app.autodiscover_tasks()
# 解决celery不能root用户启动的问题
platforms.C_FORCE_ROOT = True

# 解决时区问题
# #app.now = datetime.now
# app.now = timezone.now

# 有些情况下可以防止死锁
CELERYD_FORCE_EXECV = True
