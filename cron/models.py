# -*- coding: utf-8 -*-
from django.db import models
from django_celery_beat import models as beat_models

##定时任务信息
##与celery-beat crontabb表关联
class oratk_cron_time_info(models.Model):
    type_id = models.ForeignKey(beat_models.CrontabSchedule, to_field='id', on_delete=models.CASCADE)
    name = models.CharField(max_length=32)
    type = models.CharField(max_length=20)

##监控项，监控点信息
class oratk_cron_point_info(models.Model):
    name = models.CharField(max_length=32)
    desc = models.CharField(max_length=3200)
    sqltext = models.CharField(max_length=6200)
    point_type = models.CharField(max_length=64,null=True)
    limit = models.CharField(max_length=8)
    status = models.CharField(max_length=64)
    remake = models.CharField(max_length=64)
    crtime = models.DateField(auto_now_add=True)
    chtime = models.DateField(auto_now=True)

##task中定义任务模板信息
class oratk_cron_task_info(models.Model):
    name = models.CharField(max_length=32)
    args = models.CharField(max_length=2,null=True)
    kwargs = models.CharField(max_length=2,null=True)
    args_num =  models.CharField(max_length=2,null=True)
    crtime = models.DateField(auto_now_add=True,null=True)
    chtime = models.DateField(auto_now=True,null=True)
    type = models.CharField(max_length=32,null=True)

##任务信息
class oratk_cron_task(models.Model):
    name = models.ForeignKey(beat_models.PeriodicTask, to_field='name', on_delete=models.CASCADE)
    task_info = models.CharField(max_length=32)
    task_time = models.CharField(max_length=32)
    task_point = models.CharField(max_length=32)
    task_type = models.CharField(max_length=32,null=True)
    limit_status = models.CharField(max_length=2)
    limit = models.CharField(max_length=32,null=True)
    limit_unit = models.CharField(max_length=64,null=True)
    sms_status = models.CharField(max_length=2)
    sms_conent = models.CharField(max_length=320,null=True)
    sms_contact = models.CharField(max_length=320,null=True)
    sms_contact_rec = models.CharField(max_length=320,null=True)
    mail_status = models.CharField(max_length=2,null=True)
    mail_status_rec = models.CharField(max_length=320,null=True)
    instance = models.CharField(max_length=320,null=True)
    remake = models.CharField(max_length=32)
    email_conent = models.CharField(max_length=320, null=True)
    email_contact = models.CharField(max_length=320, null=True)
    chtime = models.CharField(max_length=32,null=True)
    crtime = models.DateTimeField(auto_now_add=True)



#短信发送情况
class oratk_cron_sms_send(models.Model):
    task_name = models.CharField(max_length=32)
    sms_conent = models.CharField(max_length=320)
    sms_contact = models.CharField(max_length=320)
    sms_status = models.CharField(max_length=32,null=True)
    sms_code = models.CharField(max_length=32,null=True)
    sms_text = models.CharField(max_length=3200,null=True)
    crtime = models.DateTimeField(auto_now_add=True)


#任务执行情况
class oratk_cron_exec_info(models.Model):
    task_batch = models.IntegerField(null=True)
    task_name = models.CharField(max_length=32)
    instance = models.CharField(max_length=320,null=True)
    sql_result = models.CharField(max_length=320,null=True)
    error_msg = models.CharField(max_length=320,null=True)
    point_name = models.CharField(max_length=32,null=True)
    limit_status = models.CharField(max_length=32,null=True)
    limit = models.CharField(max_length=32,null=True)
    limit_unit = models.CharField(max_length=64, null=True)
    sms_info = models.CharField(max_length=320,null=True)
    remake = models.CharField(max_length=320,null=True)
    crtime = models.DateTimeField(auto_now_add=True)




##ora 监控项，监控点信息
class ora_point_info(models.Model):
    name = models.CharField(max_length=32,null=True)         #名称
    desc = models.CharField(max_length=3200,null=True)       #描述
    sqltext = models.CharField(max_length=6200,null=True)    #sql文本
    args_mark = models.CharField(max_length=32,null=True)    # 是否有参数
    args_count = models.CharField(max_length=32,null=True)   # 参数个数
    #args_type = models.CharField(max_length=32,null=True)    # 参数类型
    #args_status = models.CharField(max_length=32,null=True)  # 参数状态
    args_val = models.CharField(max_length=32,null=True)     # 参数内容
    exec_type = models.CharField(max_length=32,null=True)    #执行方式
    remark = models.CharField(max_length=64,null=True)       #备注
    status = models.CharField(max_length=32,null=True)       #状态
    crtime = models.DateTimeField(auto_now_add=True,null=True)
    chtime = models.DateTimeField(auto_now=True,null=True)


##ora shell脚本内容更改记录
class ora_shell_content_version(models.Model):
    server_ip = models.CharField(max_length=32,null=True) #服务器IP
    server_port = models.CharField(max_length=32,null=True)     #服务器 port
    shell_path = models.CharField(max_length=3200,null=True)  #脚本路径
    change_show = models.CharField(max_length=3200,null=True)  #更改内容描述
    shell_content = models.TextField(null=True) #脚本内容
    remake = models.CharField(max_length=64,null=True)       #备注
    crtime = models.DateTimeField(auto_now_add=True,null=True)  #创建时间




##########巡检相关表###################
#巡检记录表
class oratk_monitor_immediate(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  #批次id
    start_time = models.CharField(max_length=32, null=True)  #开始时间
    end_time = models.CharField(max_length=32, null=True)   #结束时间
    sub_time  = models.CharField(max_length=32, null=True)  #耗时
    db_name = models.CharField(max_length=32,null=True)       #数据库名字
    db_conn = models.CharField(max_length=3200,null=True)       #数据库连接串
    remark = models.CharField(max_length=64,null=True)       #备注
    status = models.CharField(max_length=32, null=True)  #巡检执行状态
    crtime = models.DateField(auto_now_add=True,null=True)   ##创建时间

###巡检详情表
#操作系统
class oratk_monitor_info_os(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  #批次id
    monitor_time = models.CharField(max_length=32, null=True)       #巡检时间
    os_name = models.CharField(max_length=32, null=True)            #os名字
    os_bit  = models.CharField(max_length=32, null=True)            #位数
    computer_type = models.CharField(max_length=32, null=True)      #虚拟机还是物理机
    crtime = models.DateField(auto_now_add=True,null=True)          ##创建时间

#CPU信息
class oratk_monitor_info_cpu(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  #批次id
    monitor_time = models.CharField(max_length=32, null=True)       #巡检时间
    cpu_count = models.CharField(max_length=32, null=True)          #cpu个数
    cpu_core_physics  = models.CharField(max_length=32, null=True)  #物理核心数
    cpu_core_logic = models.CharField(max_length=32,null=True)      #逻辑核心数
    use_rate_perc = models.CharField(max_length=32,null=True)            #cpu使用率
    load_1 = models.CharField(max_length=32, null=True)             #1分钟load
    load_5 = models.CharField(max_length=32, null=True)             #5分钟load
    crtime = models.DateField(auto_now_add=True,null=True)          ##创建时间

#disk信息 (空间和inode)
class oratk_monitor_info_disk(models.Model):
    batch_id = models.CharField(max_length=32, null=True)            #批次id
    monitor_time = models.CharField(max_length=32, null=True)        #巡检时间
    dir_path = models.CharField(max_length=32, null=True)            #目录
    dir_space_g = models.CharField(max_length=32, null=True)           #目录大小
    space_use_rate_perc = models.CharField(max_length=32,null=True)       #空间使用率
    inode_count = models.CharField(max_length=32,null=True)          #inode总数
    inode_use_rate_perc = models.CharField(max_length=32,null=True)       #inode使用率
    crtime = models.DateField(auto_now_add=True,null=True)           ##创建时间

#disk_io
class oratk_monitor_info_disk_io(models.Model):
    batch_id = models.CharField(max_length=32, null=True)       #批次id
    monitor_time = models.CharField(max_length=32, null=True)   #巡检时间
    disk_name = models.CharField(max_length=320,null=True)      #磁盘名字
    r_exec_count = models.CharField(max_length=32,null=True)    #每秒写次数
    w_exec_count = models.CharField(max_length=32, null=True)   #每秒读次数
    r_space_mb_s = models.CharField(max_length=32,null=True)         #每秒读大小
    w_space_mb_s = models.CharField(max_length=32,null=True)         #每秒读大小
    io_await_ms = models.CharField(max_length=32,null=True)        #平均每次IO请求的处理时间(毫秒为单位)
    io_svctm_ms = models.CharField(max_length=32,null=True)        #平均每次IO请求等待时间(包括等待时间和处理时间，毫秒为单位)
    io_util_perc = models.CharField(max_length=32,null=True)         #采用周期内用于IO操作的时间比率，即IO队列非空的时间比率
    crtime = models.DateField(auto_now_add=True,null=True)      ##创建时间

#mem信息
class oratk_monitor_info_mem(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  #批次id
    monitor_time = models.CharField(max_length=32, null=True)          #开始时间
    mem_total_kb = models.CharField(max_length=32, null=True)           #mem总大小
    mem_use_rate_perc = models.CharField(max_length=32, null=True)        #men使用率
    swap_total_kb = models.CharField(max_length=32,null=True)           #swap总大小
    swap_use_rate_perc = models.CharField(max_length=32,null=True)        #swap使用率
    hugepage_total_kb = models.CharField(max_length=32,null=True)      #大页信息
    hugepage_free_kb = models.CharField(max_length=32,null=True)      #大页信息
    hugepagesize_kb = models.CharField(max_length=32,null=True)      #大页信息
    crtime = models.DateField(auto_now_add=True,null=True)           ##创建时间

#network信息
class oratk_monitor_info_network(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  # 批次id
    monitor_time = models.CharField(max_length=32, null=True)          # 开始时间
    interface_name = models.CharField(max_length=32, null=True)             #接口名称
    up_kbyte_s = models.CharField(max_length=32, null=True)          # 上传速度
    down_kbyte_s = models.CharField(max_length=32, null=True)         # 下载速度
    crtime = models.DateField(auto_now_add=True, null=True)          ##创建时间

#巡检报告
class oratk_monitor_info_report(models.Model):
    batch_id = models.CharField(max_length=32, null=True)            #批次id
    start_time = models.CharField(max_length=32, null=True)          # 开始时间
    monitor_name = models.CharField(max_length=32, null=True)          #监控名字
    use_rate = models.CharField(max_length=32, null=True)          # 使用率
    limit = models.CharField(max_length=32, null=True)                # 阀值
    crtime = models.DateField(auto_now_add=True, null=True)          ##创建时间

#err_record
class oratk_monitor_info_err(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  #批次id
    monitor_name = models.CharField(max_length=3200, null=True)      #监控名字
    command_name = models.CharField(max_length=3200, null=True)      #使用的命令
    err_info = models.CharField(max_length=3200, null=True)          # 错误信息
    crtime = models.DateTimeField(auto_now_add=True, null=True)          ##创建时间



# monitor sql监控记录
class oratk_monitor_sql_record(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  # 批次id
    monitor_name = models.CharField(max_length=3200, null=True)  # 监控名字
    monitor_record = models.TextField(null=True)  # 使用的命令
# monitor host监控记录
class oratk_monitor_host_record(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  # 批次id
    monitor_name = models.CharField(max_length=3200, null=True)  # 监控名字
    monitor_record = models.TextField(null=True)  # 使用的命令



# monitor 报告使用的阀值模板
class oratk_monitor_sql_host_limit(models.Model):
    monitor_name = models.CharField(max_length=320, null=True)  # 监控名字
    monitor_point = models.CharField(max_length=320, null=True)  # 监控项
    limit_values = models.CharField(max_length=320,null=True)  # 阀值
    monitor_type = models.CharField(max_length=32, null=True)  #类别 主机或数据库
    remark = models.CharField(max_length=320, null=True)  # 备注
    class Meta:
        unique_together = ("monitor_name", "monitor_point")



# monitor (生成report时所用阀值信息)
class oratk_monitor_report_exec_sql_limit(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  # 批次id
    db_name = models.CharField(max_length=32, null=True)  # 数据库名字
    monitor_name = models.CharField(max_length=320, null=True)  # 监控名字
    monitor_point = models.CharField(max_length=320, null=True)  # 监控项
    limit_values = models.CharField(max_length=320,null=True)  # 阀值
    remark = models.CharField(max_length=320, null=True)  # 备注


# monitor 巡检报告（report)
class oratk_monitor_report(models.Model):
    batch_id = models.CharField(max_length=32, null=True)  # 批次id
    monitor_name = models.CharField(max_length=3200, null=True)  # 监控名字
    monitor_point = models.CharField(max_length=3200, null=True)  # 监控名字
    monitor_record = models.TextField(null=True)  # 使用的命令
    limit_values = models.CharField(max_length=32, null=True)  # 阀值
    monitor_type = models.CharField(max_length=32, null=True)  # 监控类型
    monitor_make_time = models.CharField(max_length=32,null=True) #生成巡检报告时间
    host_limit = models.CharField(max_length=3200, null=True)  # host的阀值统计
    seq_record = models.CharField(max_length=32,null=True) #插入的序号


# oratk自定义查询保留sql
class oratk_query_savesql(models.Model):
    name = models.CharField(max_length=32,null=True)         #名称
    sqltext = models.CharField(max_length=6200,null=True)    #sql文本
    remark = models.CharField(max_length=64,null=True)       #备注
    crtime = models.DateTimeField(auto_now_add=True,null=True)


# 保存scripts中 args_list
class oratk_script_args(models.Model):
    args_values = models.CharField(max_length=6200,null=True)         #args_list
    crtime = models.DateTimeField(auto_now_add=True,null=True)       #插入时间


# awr & ash
class oratk_awr_ash_report(models.Model):
    dbinfo = models.CharField(max_length=32, null=True)
    conn_str = models.CharField(max_length=32, null=True)
    report_type = models.CharField(max_length=32, null=True)
    report_format = models.CharField(max_length=32, null=True)
    snapshot_range = models.CharField(max_length=32, null=True)
    date_range = models.CharField(max_length=64, null=True)
    report_content = models.TextField(null=True)
    inst_type = models.CharField(max_length=64,null=True)
    crtime = models.DateTimeField(auto_now_add=True,null=True)